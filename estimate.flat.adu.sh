#!/bin/bash

# Title	      : estimate.flat.adu.sh 
# Date	      : Sun 30 Oct 2016 04:08:38 PM CET
# Version     : 0.0.1
# Author      : "Thomas Stibor" <thomas@stibor.net>
# Description : Take raw images starting with shortest possible exposure time
#               and calculate with program aducount.cpp the relative ADU count
#               for each image by increasing the exposure time. That is
#		add up ADU's for each pixel for R,G,B,G2 and divide the sum by 2^14.
#               Stop when ADU count is closest to 0.5, this is a proper value for flats.

source libgphoto.sh
source libsettings.sh

IFS=$'\n' # Make newlines the only separator.

# Return absolute value
# Parameters: ${1} input value
__abs() {
    if (( $(echo "${1} <= 0" | bc -l) )); then
	echo "-1 * ${1}" | bc -l | sed -r 's/^(-?)./\10./' # Leading zero
    else
	echo "${1}" | bc -l | sed -r 's/^(-?)./\10./'
    fi
}

#####################################################
# main
#####################################################
GPHOTO2_BIN=`which gphoto2`
ADUCOUNT_BIN="bin/aducount"
__check_bin ${GPHOTO2_BIN} "gphoto2"
__check_bin ${ADUCOUNT_BIN} "bin/aducount"
__gphoto_set_config "imageformat=9"

declare -a EXPMAP
# Store in associative array EXPMAP[exposure time id]=[exposure time]
for exptime in `${GPHOTO2_BIN} --get-config shutterspeed`; do
    if [ `echo $exptime | grep "Choice:"` ]; then
	KEY=`echo $exptime | awk '/Choice:/{print $2}'`
	VALUE=`echo $exptime | awk '/Choice:/{print $3}'`
	EXPMAP["${KEY}"]="${VALUE}"
    fi
done

BEST_DIFF="1024"

# Iterate in reverse order, thus starting with shortest exposure time.
for (( s=${#EXPMAP[@]}-1 ; s>=0 ; s-- )) ; do
    FILENAME="/tmp/flat_find_adu_${s}.cr2"
    echo ${FILENAME}
    __gphoto_set_config shutterspeed="${EXPMAP[s]}"
    __gphoto_exp ${FILENAME}
    ADU=`${ADUCOUNT_BIN} ${FILENAME} | awk '/adu:/{ print $2 }'`
    echo "ADU: ${ADU}"

    DIFF="`echo "0.5 - ${ADU}" | bc -l | sed -r 's/^(-?)./\10./'`"
    DIFF=`__abs "${DIFF}"`

    # ${ADU} value monotonically increases, thus stop when abs(0.5 - ${ADU}) increases.
    if (( $(echo "${DIFF} < ${BEST_DIFF}" | bc -l) )); then
	BEST_DIFF=${DIFF}
    else
	echo "found best value at adu at shutterspeed ${EXPMAP[$((s-1))]}"
	exit 1
    fi
done
