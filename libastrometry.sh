#!/bin/bash

PATH=$PATH:/usr/local/astrometry/bin

__solve_img() {
    local AN_TMP_OUTPUT="/tmp/an.result.txt"
    local AN_BIN=$1
    local AN_PARAM_STD=$2
    local AN_APP_L=$3
    local AN_APP_H=$4
    local AN_DOWNSAMPLE=$5
    local AN_CPULIMIT=$6
    local IMG_FILENAME=$7
    local IMG_SIZE=`feh -l ${IMG_FILENAME} | awk '{ if (NR==2) print $3 "x" $4}'`
    
    [[ ! -f ${AN_TMP_OUTPUT} ]] || rm -rf ${AN_TMP_OUTPUT}
    ${AN_BIN} ${AN_PARAM_STD} ${AN_APP_L} ${AN_APP_H} ${AN_DOWNSAMPLE} ${AN_CPULIMIT} ${IMG_FILENAME} \
	| tee -a ${AN_TMP_OUTPUT} \
	| yad --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 \
	      --title="Platesolving image ${IMG_FILENAME}: ${IMG_SIZE}" \
	      --center \
	      --gtkrc="./skytools_gtk.rc" \
	      --text="${AN_BIN} ${AN_PARAM_STD} ${AN_APP_L} ${AN_APP_H} ${AN_DOWNSAMPLE} ${AN_CPULIMIT} ${IMG_FILENAME}"
    # echo `cat ${AN_TMP_OUTPUT} | grep -Po 'Field center:\s\(RA,Dec\).*\(\K[^)]+' | tr -d ','`
    local RESULT=`cat ${AN_TMP_OUTPUT} | grep -Po 'Field center:\s\(RA,Dec\).*\(\K[^)]+' | tr -d ','`
    if [[ ! -z ${RESULT} ]]
    then
	echo ${RESULT}
    else
	echo ""
	{ ERRMSG="Platesolving could not find a solution and failed"; \
	  yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
	      --center \
	      --gtkrc="./skytools_gtk.rc" \
	      --text="${ERRMSG}"; exit 1; }
    fi
} 
