#!/bin/bash
# Title       : polaralign.error.mount.sh
# Date        : Tue 09 Feb 2016 06:36:21 PM CET
# Version     : 0.0.1
# Author      : "Thomas Stibor" <thomas@stibor.net>
# Description : 1.) Slew to 3 predefined RA,DEC coordinates.
#               2.) Take at each RA,DEC coordinates an image and solve it.
#               3.) Use the solved RA,DEC coordinates to calculate the polar aligment error.

source libgphoto.sh
source libsettings.sh
source libastrometry.sh

__mount_ctrl_goto_radec() {
    ${MOUNTCTRL_BIN} "/dev/ttyUSB0" "goto" ${1} ${2}
}

__mount_ctrl_goto_cancel() {
    ${MOUNTCTRL_BIN} "/dev/ttyUSB0" "gotocancel"
}

__mount_ctrl_goto_in_progress() {
    ${MOUNTCTRL_BIN} "/dev/ttyUSB0" "gotoinprogress"
}

# main
GPHOTO2_BIN=`which gphoto2`
__check_bin ${GPHOTO2_BIN} "gphoto2"

AN_BIN=`which solve-field`
__check_bin ${AN_BIN} "solve-field"

MOUNTCTRL_BIN="bin/mountctrl"
__check_bin ${MOUNTCTRL_BIN} "bin/mountctrl"

GPHOTO2_EXP_SEC=$(__read_settings "selected_exposure")
GPHOTO2_ISO=$(__read_settings "selected_iso")
AN_DOWNSAMPLE=" --downsample "$(__read_settings "selected_downsample")
AN_CPULIMIT=" --cpulimit "$(__read_settings "selected_cpulimit")
AN_PARAM_STD="--no-plots --no-fits2fits --no-verify --overwrite --scale-units degwidth --scale-low 0.1 --scale-high 180.0 --tweak-order 2"

######## Set camera settings ########
__gphoto_set_config "imageformat=6"
__gphoto_set_config "shutterspeed=bulb"
__gphoto_set_config "iso=${GPHOTO2_ISO}"

RA_SOLVED_LIST=("" "" "")
DEC_SOLVED_LIST=("" "" "")

RA_GOTO_LIST=("RA Goto  1" "RA Goto  2" "RA Goto  3")
DEC_GOTO_LIST=("DEC Goto 1" "DEC Goto 2" "DEC Goto 3")

NUM_SOLVED_IMAGES=0
while [ $NUM_SOLVED_IMAGES -lt 3 ]
do
    result=$(yad --undecorated --title="Solve and Sync" --form --item-separator="," \
		 --center \
		 --gtkrc="./skytools_gtk.rc" \
		 --field="Goto RA,DEC and take image":CB $((${NUM_SOLVED_IMAGES}+1)) \
		 --field="${RA_GOTO_LIST[0]}" "160.00" \
		 --field="${DEC_GOTO_LIST[0]}" "8.00"\
		 --field="${RA_GOTO_LIST[1]}" "220.00" \
		 --field="${DEC_GOTO_LIST[1]}" "8.00" \
		 --field="${RA_GOTO_LIST[2]}" "280.00" \
		 --field="${DEC_GOTO_LIST[2]}" "8.00" \
		 --field="Number of solved images":RO "${NUM_SOLVED_IMAGES}" \
		 --button="Slew and Solve:2" \
		 --button="Cancel Slewing:3" \
		 --button="gtk-cancel:1" )

    ret=$?
    [[ $ret -eq 1 ]] && exit 1

    # Cancel goto slew command.
    [[ $ret -eq 3 ]] && { ERRMSG=$(__mount_ctrl_goto_cancel); ret=$?; 
			  [[ $ret -eq 1 ]] && { yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
						    --gtkrc="./skytools_gtk.rc" \
						    --text="${ERRMSG}"; exit 1; }; }

    array=(${result//|/ })
    IMG_CB_SELECTED=${array[0]}
    PICKED_IDX=0
    RA=0
    DEC=0
    [[ $ret -eq 2 ]] && [[ ${IMG_CB_SELECTED} == "1" ]] && { PICKED_IDX=1; RA=${array[1]}; DEC=${array[2]}; }
    [[ $ret -eq 2 ]] && [[ ${IMG_CB_SELECTED} == "2" ]] && { PICKED_IDX=1; RA=${array[3]}; DEC=${array[4]}; }
    [[ $ret -eq 2 ]] && [[ ${IMG_CB_SELECTED} == "3" ]] && { PICKED_IDX=1; RA=${array[5]}; DEC=${array[6]}; }

    [[ ${PICKED_IDX} -gt 0 ]] &&
    	{ # echo "COORD: ${RA} ${DEC}";
    	    ERRMSG=$(__mount_ctrl_goto_radec "${RA}" "${DEC}"); 
    	    ret=$?
    	    [[ $ret -eq 1 ]] && { yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
    				      --gtkrc="./skytools_gtk.rc" \
    				      --text="${ERRMSG}"; exit 1; }
    	}

    GOTO_IN_PROGRESS=1
    while [ ${GOTO_IN_PROGRESS} -eq 1 ]
    do
    	ERRMSG=$(__mount_ctrl_goto_in_progress)
    	ret=$?
    	[[ $ret -eq 253 ]] && { yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
    				    --gtkrc="./skytools_gtk.rc" \
    				    --text="${ERRMSG}"; exit 1; }
    	[[ $ret -eq 1 ]] && { echo "Goto is in progress"; sleep 1; }
    	[[ $ret -eq 0 ]] && { echo "Goto finished"; GOTO_IN_PROGRESS=0; }
    done
		  
    # Finally we can take our image.
    IMG_FILENAME="/tmp/${IMG_CB_SELECTED}_tosolve.jpg"
    __gphoto_bulb "${GPHOTO2_EXP_SEC}" ${IMG_FILENAME}

    ######## Solve image with astrometry.net ########
    RA_DEC=$(__solve_img "${AN_BIN}" "${AN_PARAM_STD}" "${AN_DOWNSAMPLE}" "${AN_CPULIMIT}" "${IMG_FILENAME}")
    [[ -z ${RA_DEC} ]] && exit 1
    # echo "RA,DEC: ${RA_DEC}"
    RA_DEC=(${RA_DEC//' '/ })

    RA_SOLVED_LIST[${IMG_CB_SELECTED}-1]=${RA_DEC[0]}
    DEC_SOLVED_LIST[${IMG_CB_SELECTED}-1]=${RA_DEC[1]}
    NUM_SOLVED_IMAGES=$(( $NUM_SOLVED_IMAGES + 1 ))

    RA_GOTO_LIST[${IMG_CB_SELECTED}-1]="${RA_GOTO_LIST[${IMG_CB_SELECTED}-1]} (${RA_DEC[0]})"
    DEC_GOTO_LIST[${IMG_CB_SELECTED}-1]="${DEC_GOTO_LIST[${IMG_CB_SELECTED}-1]} (${RA_DEC[1]})"
done

ALIGN_ERROR=`${MOUNTCTRL_BIN} alignerror ${RA_SOLVED_LIST[0]} ${DEC_SOLVED_LIST[0]} ${RA_SOLVED_LIST[1]} ${DEC_SOLVED_LIST[1]} ${RA_SOLVED_LIST[2]} ${DEC_SOLVED_LIST[2]}`
echo "`date`" >> alignment.error.txt
for i in {0..2}; do
    echo "$i : (${RA_SOLVED_LIST[$i]}, ${DEC_SOLVED_LIST[$i]})" >> alignment.error.txt
done
echo ${ALIGN_ERROR} >> alignment.error.txt

yad --image=info \
    --width=400 \
    --title='Polar Aligment Error' \
    --button="gtk-close:1" \
    --gtkrc="./skytools_gtk.rc" \
    --text="${ALIGN_ERROR}"
