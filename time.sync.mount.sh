#!/bin/bash

source libsettings.sh

__mount_ctrl_sync_time() {
    ${MOUNTCTRL_BIN} "/dev/ttyUSB0" "synctime"
}

MOUNTCTRL_BIN="bin/mountctrl"
__check_bin ${MOUNTCTRL_BIN} "bin/mountctrl"

result=$(yad --undecorated --title="Sync Time" --form --item-separator="," \
	     --button="Sync Time:2" \
             --button="gtk-cancel:1" \
	     --gtkrc="./skytools_gtk.rc")

ret=$?
[[ $ret -eq 1 ]] && exit 1

ERRMSG=$(__mount_ctrl_sync_time)
ret=$?
[[ $ret -eq 1 ]] && { yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
                          --gtkrc="./skytools_gtk.rc" \
			  --text="${ERRMSG}"; exit 1; }

{ yad --button="gtk-ok:0" --image=info \
      --center \
      --gtkrc="./skytools_gtk.rc" \
      --text="Successfully send PC time to mount"; exit 0; }
