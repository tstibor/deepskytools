#!/bin/bash

source libsettings.sh

__mount_ctrl_sync_location() {
    ${MOUNTCTRL_BIN} "/dev/ttyUSB0" "synclocation" $1 $2
}

MOUNTCTRL_BIN="bin/mountctrl"
__check_bin ${MOUNTCTRL_BIN} "bin/mountctrl"

NAME_LOCATION_LIST=("Odenwald" "Himbach")
LAT_LOCATION_LIST=("49.799383" "50.263172")
LON_LOCATION_LIST=("8.993743" "8.996894")

result=$(yad --undecorated --title="Sync Location" --form --item-separator="," \
	     --field="Location":CB `echo ${NAME_LOCATION_LIST[@]} | tr ' ' ','` \
	     --button="Sync Location:2" \
             --button="gtk-cancel:1" \
	     --gtkrc="./skytools_gtk.rc")

ret=$?
[[ $ret -eq 1 ]] && exit 1
array=(${result//|/ })

for i in "${!NAME_LOCATION_LIST[@]}"
do
    [[ ${NAME_LOCATION_LIST[i]} == ${array[0]} ]] && break
done

ERRMSG=$(__mount_ctrl_sync_location ${LAT_LOCATION_LIST[i]} ${LON_LOCATION_LIST[i]})
ret=$?
[[ $ret -eq 1 ]] && { yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
                          --gtkrc="./skytools_gtk.rc" \
                          --text="${ERRMSG}"; exit 1; }

{ yad --button="gtk-ok:0" --image=info \
      --center \
      --gtkrc="./skytools_gtk.rc" \
      --text="Successfully sent location : ${NAME_LOCATION_LIST[i]}\n(Lat,Lon): (${LAT_LOCATION_LIST[i]},${LON_LOCATION_LIST[i]}) to mount"; exit 0; }



