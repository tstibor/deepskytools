/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2016, Thomas Stibor <thomas@stibor.net>
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <nexstar.h>

#define ERROR 1
#define SUCCESS 0

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

static int dev = 0;

static inline double sind(const double val)
{
    return sin(val / 180.0 * M_PI);
}

static inline double cosd(const double val)
{
    return cos(val / 180.0 * M_PI);
}

static inline double atand(const double val)
{
    return (atan(val) / M_PI * 180);
}

void alignment_error(const double const ra[3], const double const dec[3],
		     double *r_east_west_error, double *r_north_south_error)
{
    double v1[3] = {0};
    double v2[3] = {0};
    double v3[3] = {0};

    v1[0] = sind(ra[0]) * cosd(dec[0]);
    v1[1] = cosd(ra[0]) * cosd(dec[0]);
    v1[2] = sind(dec[0]);

    v2[0] = sind(ra[1]) * cosd(dec[1]);
    v2[1] = cosd(ra[1]) * cosd(dec[1]);
    v2[2] = sind(dec[1]);
    
    v3[0] = sind(ra[2]) * cosd(dec[2]);
    v3[1] = cosd(ra[2]) * cosd(dec[2]);
    v3[2] = sind(dec[2]);
    
    double a[3] = {0};
    double b[3] = {0};

    for (unsigned char i = 0; i < 3; i++) {
	a[i] = v2[i] - v1[i];
	b[i] = v3[i] - v1[i];
    }
    
    double cross_product[3] = {0};
    cross_product[0] = a[1] * b[2] - a[2] * b[1];
    cross_product[1] = a[2] * b[0] - a[0] * b[2];
    cross_product[2] = a[0] * b[1] - a[1] * b[0];
    
    double norm  = sqrt((cross_product[0] * cross_product[0]) +
			(cross_product[1] * cross_product[1]) +
			(cross_product[2] * cross_product[2]));
    for (unsigned int i = 0; i < 3; i++)
	cross_product[i] = cross_product[i] / norm;

    *r_east_west_error = atand(cross_product[0]);
    *r_north_south_error = atand(cross_product[1]);

    fprintf(stdout, "East-West direction polar alignment error: %f arc minutes, %f degrees\n\n",
	    atand(cross_product[0]) * 60, atand(cross_product[0]));
    fprintf(stdout, "North-South direction polar alignment error: %f arc minutes, %f degrees\n",
	    atand(cross_product[1]) * 60, atand(cross_product[1]));
}

int mount_sync_time()
{
    int rc;
    time_t t = time(NULL);
    struct tm *tm = localtime(&t);
    int tz = (int)tm->tm_gmtoff / 3600; /* tm->tm_gmtoff = Seconds east of UTC. */
    int dst = tm->tm_isdst;

    if (dst < 0)		/* Daylight saving time is not available. */
	dst = 0;		/* Set daylight saving time is not in effect. */

    rc = tc_set_time(dev, t, tz, dst);
    if (rc < 0) {
        fprintf(stdout, "%s:%d\n", "tc_set_time", rc);
        return ERROR;
    }
    return SUCCESS;
}

int mount_sync_radec(const double ra, const double dec)
{
    int rc;
    rc = tc_sync_rade_p(dev, ra, dec);
    if (rc < 0) {
	fprintf(stdout, "%s:%d", "tc_sync_rade_p\n", rc);
	return ERROR;
    }
    return SUCCESS;
}

int mount_goto_radec(const double ra, const double dec)
{
    int rc;
    rc = tc_goto_rade_p(dev, ra, dec);
    if (rc < 0) {
	fprintf(stdout, "%s:%d", "tc_sync_goto_p\n", rc);
	return ERROR;
    }
    return SUCCESS;
}

int mount_goto_cancel()
{
    int rc;
    rc = tc_goto_cancel(dev);
    if (rc < 0) {
	fprintf(stdout, "%s:%d", "tc_goto_cancel\n", rc);
	return ERROR;
    }
    return SUCCESS;
}

int mount_goto_in_progress()
{
    int rc;
    rc = tc_goto_in_progress(dev);
    if (rc < 0) {
	fprintf(stdout, "%s:%d", "tc_goto_in_progress\n", rc);
	return 235;		/* In bash scripts we have no negative return values. */
    }
    return rc;			/* 0 = is not in progress,
				   1 = is in progress.*/
}

int mount_sync_location(const double lat, const double lon)
{
    int rc;

    rc = tc_set_location(dev, lon, lat);
    if (rc < 0) {
	fprintf(stdout, "%s:%d", "tc_set_location\n", rc);
	return ERROR;
    }
    return SUCCESS;
}

void usage(const char *prgname)
{
    const int len = strlen(prgname);
    
    printf("%s <usbdev> synctime\n"
	   "%*s" "<usbdev> syncradec <ra> <dec>\n"
	   "%*s" "<usbdev> goto <ra> <dec>\n"
	   "%*s" "<usbdev> gotocancel\n"
	   "%*s" "<usbdev> gotoinprogress\n"
	   "%*s" "<usbdev> synclocation <lat> <lon>\n"
	   "%*s" "alignerror <ra1> <dec1> <ra2> <dec2> <ra3> <dec3>\n",
	   prgname,
	   len + 1, " ",
	   len + 1, " ",
	   len + 1, " ",
	   len + 1, " ",
	   len + 1, " ",
	   len + 1, " ");
}

int main(int argc, char *argv[])
{
    int rc = ERROR;
    /* <usbdev> synctime */
    /* <usbdev> syncradec <ra> <dec> */
    /* <usbdev> goto <ra> <dec> */
    /* <usbdev> gotocancel */
    /* <usbdev> gotoinprogress */
    /* <usbdev> synclocation <lat> <lon> */
    /* alignerror <ra1> <dec1> <ra2> <dec2> <ra3> <dec3> */
    
    if (argc < 2) {
	usage(argv[0]);
	return rc;
    }
    
    if (argc == 8 && !strcmp(argv[1], "alignerror")) {
      const double const ra[] = {strtod(argv[2], NULL), strtod(argv[4], NULL), strtod(argv[6], NULL)};
      const double const dec[] = {strtod(argv[3], NULL), strtod(argv[5], NULL), strtod(argv[7], NULL)};
      double r_east_west_error;
      double r_north_south_error;
      
      alignment_error(ra, dec, &r_east_west_error, &r_north_south_error);
      return SUCCESS;
    }
    
    dev = open_telescope(argv[1]);
    if (dev < 0) {
	fprintf(stdout, "can not open device: %s\n", argv[1]);
	rc = ERROR;
	goto clean_up;
    }
    if (argc == 3 && !strcmp(argv[2], "synctime")) {
	rc = mount_sync_time();
	goto clean_up;
    }
    if (argc == 5 && !strcmp(argv[2], "syncradec")) {
	rc = mount_sync_radec(strtod(argv[3], NULL), strtod(argv[4], NULL));
	goto clean_up;
    }
    if (argc == 5 && !strcmp(argv[2], "goto")) {
	rc = mount_goto_radec(strtod(argv[3], NULL), strtod(argv[4], NULL));
	goto clean_up;
    }
    if (argc == 5 && !strcmp(argv[2], "synclocation")) {
	rc = mount_sync_location(strtod(argv[3], NULL), strtod(argv[4], NULL));
	goto clean_up;
    }
    if (argc == 3 && !strcmp(argv[2], "gotocancel")) {
	rc = mount_goto_cancel();
	goto clean_up;
    }
    if (argc == 3 && !strcmp(argv[2], "gotoinprogress")) {
	rc = mount_goto_in_progress();
	goto clean_up;
    }

 clean_up:
    close_telescope(dev);
    return rc;
}
