#include "libraw/libraw.h"

// Canon 700D has 14 Bit RAW
#define MAX_ADU_VAL 16384

typedef struct {
	unsigned long long r;
	unsigned long long g;
	unsigned long long b;
	unsigned long long g2;
	unsigned long long max_r;
	unsigned long long max_g;
	unsigned long long max_b;
	unsigned long long max_g2;
	unsigned long long max_rgbg2;
} adu_t;


void process_image(const char *file)
{
        LibRaw iProcessor;

        iProcessor.open_file(file);
        printf("image size: %d x %d\n",iProcessor.imgdata.sizes.width,iProcessor.imgdata.sizes.height);
        iProcessor.unpack();
        iProcessor.raw2image();

	unsigned long long wh = iProcessor.imgdata.sizes.iwidth * iProcessor.imgdata.sizes.iheight;

	adu_t adu;
	memset(&adu, 0, sizeof(adu));
	adu.max_rgbg2 = wh * MAX_ADU_VAL;

        for(unsigned int i = 0; i < wh; i++) {
#if PRINT_ADU_VALUES
		printf("i=%d R=%d G=%d B=%d G2=%d\n", i,
		       iProcessor.imgdata.image[i][0],
		       iProcessor.imgdata.image[i][1],
		       iProcessor.imgdata.image[i][2],
		       iProcessor.imgdata.image[i][3]);
#endif
		// R
		if (iProcessor.imgdata.image[i][0] > adu.max_r)
			adu.max_r = iProcessor.imgdata.image[i][0];
		// G
		if (iProcessor.imgdata.image[i][1] > adu.max_g)
			adu.max_g = iProcessor.imgdata.image[i][1];
		// B
		if (iProcessor.imgdata.image[i][2] > adu.max_b)
			adu.max_b = iProcessor.imgdata.image[i][2];
		// G2
		if (iProcessor.imgdata.image[i][3] > adu.max_g2)
			adu.max_g2 = iProcessor.imgdata.image[i][3];

		adu.r += iProcessor.imgdata.image[i][0];
		adu.g += iProcessor.imgdata.image[i][1];
		adu.b += iProcessor.imgdata.image[i][2];
		adu.g2 += iProcessor.imgdata.image[i][3];
	}

	printf("max: r = %lld, g = %lld, b = %lld, g2 = %lld\n",
	       adu.max_r, adu.max_g, adu.max_b, adu.max_g2);
	printf("adu: %f\n", (adu.r +  adu.g + adu.b + adu.g2) / (double)adu.max_rgbg2);

        iProcessor.recycle();
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("missing input file\n");
		return 1;
	}

	process_image(argv[1]);

	return 0;
}
