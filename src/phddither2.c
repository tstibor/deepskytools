/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2016, Thomas Stibor <thomas@stibor.net>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <netdb.h>
#include <unistd.h>
#include <math.h>
#include <getopt.h>
#include <stdbool.h>
#include <json-c/json.h>
#include "log.h"

#define SUCCESS 0
#define DITHER_SUCCESS 0
#define DITHER_FAILED 1
#define DITHER_WAIT 254
#define PARM_ERROR 2
#define SOCK_ERROR 3

#define MAX_JSON_LENGTH 1024

#define STRCMP(s1, s2) !__builtin_strcmp(s1, s2)

static int sockfd;

int connect_socket(const char *hostname, const int portno)
{
	struct sockaddr_in serv_addr;
	struct hostent *server;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		LOG_ERROR(errno, "creating socket descriptor: %d failed", sockfd);
		return SOCK_ERROR;
	}

	server = gethostbyname(hostname);
	if (server == NULL) {
		LOG_ERROR(h_errno, "hostname lookup: %s failed", hostname);
		return SOCK_ERROR;
	}

	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		LOG_ERROR(errno, "connection on socket: %d failed", sockfd);
		return SOCK_ERROR;
	}

	return SUCCESS;
}

int sock_write_phd(const char *msg)
{
	ssize_t n;

	n = write(sockfd, msg, strlen(msg));
	if (n < 0) {
		LOG_ERROR(errno, "writing to socked descriptor: %d failed, n: %zd", sockfd, n);
		return SOCK_ERROR;
	}
	return SUCCESS;
}

int sock_read_phd(char *result)
{
	ssize_t n;

	n = read(sockfd, result, MAX_JSON_LENGTH);
	if (n < 0) {
		LOG_ERROR(errno, "reading from socket descriptor: %d failed, n: %zd", sockfd, n);
		return SOCK_ERROR;
	}

	return SUCCESS;
}

int wait_for_settle_done(json_object *jobj)
{
	/* Example json messages (errors):
	  {"jsonrpc":"2.0","error":{"code":1,"message":"Dither error"},"id":65537}
	  {"Event":"SettleDone","Timestamp":1484572593.341,"Host":"polaris","Inst":1,"Status":1,"Error":"timed-out waiting for guider to settle","TotalFrames":49,"DroppedFrames":39}

	  Example json message (success):
	  {"Event":"SettleDone","Timestamp":1484572700.803,"Host":"polaris","Inst":1,"Status":0,"TotalFrames":27,"DroppedFrames":0}
	*/

	json_object *event = NULL;
	json_object *status = NULL;
	json_object *jsonrpc = NULL;
	json_object *error = NULL;
	json_object *id = NULL;

	/* Handle "Event" */
	if (json_object_object_get_ex(jobj, "Event", &event) &&
	    STRCMP(json_object_get_string(event), "SettleDone")) {
		if (json_object_object_get_ex(jobj, "Status", &status)) {
			switch (json_object_get_int(status)) {
			case 0: {
				return DITHER_SUCCESS;
			}
			case 1: {
				json_object *failed = NULL;
				if (json_object_object_get_ex(jobj, "Error", &failed)) {
					LOG_WARN("%s", json_object_get_string(failed));
				}
				return DITHER_FAILED;
			}
			default:
				return DITHER_WAIT;
			}
		}
	} /* Handle "jsonrpc error" */
	else if (json_object_object_get_ex(jobj, "jsonrpc", &jsonrpc) &&
		 json_object_object_get_ex(jobj, "error", &error) &&
		 json_object_object_get_ex(jobj, "id", &id)) {
		if (json_object_get_int(id) == 65537)
			return DITHER_FAILED;
	}

	return DITHER_WAIT;
}

int send_dither_cmd(const float dither_pixels, const float max_gpixels,
		    const unsigned int min_time, const unsigned int timeout)
{
	char dither_cmd[MAX_JSON_LENGTH] = {0};

	sprintf(dither_cmd, "{\"method\": \"dither\", \"params\": "
		"[%f, false, {\"pixels\": %f, \"time\": %d, \"timeout\": %d}],"
		" \"id\": 65537}\r\n",
		dither_pixels, max_gpixels, min_time, timeout);
	LOG_TRACE("%s", dither_cmd);

	return sock_write_phd(dither_cmd);
}

void usage(const char* prgname)
{
	printf("usage: %s\n"
	       "\t-d, --ditherpixel   [default 10 pixel] (shift of the lock position by +/- pixels on each of the RA and DEC axes)\n"
	       "\t-s, --settledist    [default 1.5 pixel] (maximum guide distance for guiding to be considered stable or in-range)\n"
	       "\t-m, --mintimestable [default 10 sec] (minimum time to be in-range before considering guiding to be stable)\n"
	       "\t-t, --timeout       [default 60 sec] (time limit before settling is considered to have failed)\n", prgname);

	exit(SUCCESS);
}

int main(int argc, char *argv[])
{
	int rc;
	char msg[MAX_JSON_LENGTH] = {0};
	bool done = false;
	unsigned long lcnt = 1;
	json_object *jobj = NULL;

	/* Default parameters. */
	float arg_dither_pixels = 10;
	float arg_settle_dist = 1.5;
	unsigned int arg_min_time = 10;
	unsigned int arg_timeout = 60;
	int c;

	while (1) {
		static struct option long_options[] = {
			{"ditherpixel",   required_argument, 0, 'd'},
			{"settledist",    required_argument, 0, 's'},
			{"mintimestable", required_argument, 0, 'm'},
			{"timeout",       required_argument, 0, 't'},
			{"help",          no_argument,       0, 'h'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long (argc, argv, "d:s:m:t:h",
				 long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			usage(argv[0]);
			break;
		case 'd':
			arg_dither_pixels = atof(optarg);
			break;
		case 's':
			arg_settle_dist = atof(optarg);
			break;
		case 'm':
			arg_min_time = atoi(optarg);
			break;
		case 't':
			arg_timeout = atoi(optarg);
			break;
		default:
			usage(argv[0]);
		}
	}

	rc = connect_socket("localhost", 4400);
	if (rc != SUCCESS)
		goto clean_up;

	rc = send_dither_cmd(arg_dither_pixels, arg_settle_dist,
			     arg_min_time, arg_timeout);
	if (rc != SUCCESS)
		goto clean_up;

	while (!done) {
		rc = sock_read_phd(msg);
		LOG_TRACE("%lu: %s", lcnt, msg);
		jobj = json_tokener_parse(msg);
		bzero(msg, MAX_JSON_LENGTH);

		rc = wait_for_settle_done(jobj);
		switch (rc) {
		case DITHER_FAILED: {
			done = true;
			LOG_WARN("%s", "dither failed");
			break;
		}
		case DITHER_SUCCESS: {
			done = true;
			LOG_TRACE("%s", "dither successful");
			break;
		}
		case DITHER_WAIT:
		default: {};
		}
		lcnt++;
	}

clean_up:
	close(sockfd);
	return rc;
}
