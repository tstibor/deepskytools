/*
 * This program allows in a trivial way to switch
 * in circular direction between:
 * low screen brightness -> red screen -> original screen brightness.
 * This is useful when using a laptop for controlling a telescope
 * or auto guiding.
 * This program employs the X-Server tool 'xcalib' for realizing this feature.
 * Setting in the window manager a shortcut,
 * e.g. on key <Prt Scr> easily allows you to switch between the different screen modes.
 *
 * Make sure to install the package: xcalib (sudo apt-get install xcalib, on Debian).
 *
 * Version 0.1, Copyright (c) 2015, Thomas Stibor <thomas@stibor.net>
 */

#include <stdio.h>

#define RS_STATE_FILE "/tmp/state.rs"

const char const commands[3][256] = {
    /* 0: original screen brightness. */
    {"/usr/bin/xcalib -clear"},
    /* 1: low screen brightness. */
    {"/usr/bin/xcalib -clear && "	         \
     "/usr/bin/xcalib -co 30 -alter"},
    /* 2: red screen. */
    {"/usr/bin/xcalib -clear && "		 \
     "/usr/bin/xcalib -co 60 -alter && "	 \
     "/usr/bin/xcalib -green .1 0 1 -alter && "	 \
     "/usr/bin/xcalib -blue .1 0 1 -alter && "	 \
     "/usr/bin/xcalib -red 0.5 1 40 -alter"}};

int rs_state()
{
    FILE *f_state;
    FILE *f_cmd;
    int state = 0;

    f_state = fopen(RS_STATE_FILE, "r+");
    if (f_state == NULL) {
	/* If file does not exist, then create one and init with state 1. */
	f_state = fopen(RS_STATE_FILE, "w+");
	if (f_state == NULL) {
	    perror("error fopen()");
	    return 1;
	}
	fprintf(f_state, "%d", state);
    }
    fscanf(f_state, "%d", &state);
    state = (state + 1) % 3;
    fseek(f_state, 0L, SEEK_SET);
    fprintf(f_state, "%d", state);
    
    f_cmd = popen(commands[state], "r");
    if (f_cmd == NULL) {
	perror("error popen()");
	if (f_state)
	    fclose(f_state);
	return 1;
    }
    pclose(f_cmd);
    fclose(f_state);
    
    return 0;
}

int main(int argc, char *argv[])
{
    return rs_state();
}
