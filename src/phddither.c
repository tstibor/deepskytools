/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2016, Thomas Stibor <thomas@stibor.net>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <netdb.h>
#include <unistd.h>
#include <math.h>
#include <getopt.h>
#include <stdbool.h>
#include "log.h"

#define SUCCESS 0
#define DITHER_FAILED 1
#define PARM_ERROR 2
#define SOCK_ERROR 3

static int sockfd;

enum {
	MSG_PAUSE = 1,		/* 1 */
	MSG_RESUME,		/* 2 */
	MSG_MOVE1,		/* 3 */
	MSG_MOVE2,		/* 4 */
	MSG_MOVE3,		/* 5 */
	MSG_IMAGE,		/* 6 */
	MSG_GUIDE,		/* 7 */
	MSG_CAMCONNECT,		/* 8 */
	MSG_CAMDISCONNECT,	/* 9 */
	MSG_REQDIST,		/* 10 */
	MSG_REQFRAME,		/* 11 */
	MSG_MOVE4,		/* 12 */
	MSG_MOVE5,		/* 13 */
	MSG_AUTOFINDSTAR,	/* 14 */
	MSG_SETLOCKPOSITION,	/* 15 */
	MSG_FLIPRACAL,          /* 16 */
	MSG_GETSTATUS,          /* 17 */
	MSG_STOP,               /* 18 */
	MSG_LOOP,               /* 19 */
	MSG_STARTGUIDING,       /* 20 */
	MSG_LOOPFRAMECOUNT,     /* 21 */
	MSG_CLEARCAL,           /* 22 */
	MSG_FLIP_SIM_CAMERA,    /* 23 */
	MSG_DESELECT,           /* 24 */
};

int connect_socket(const char *hostname, const int portno)
{
	struct sockaddr_in serv_addr;
	struct hostent *server;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		LOG_ERROR(errno, "creating socket descriptor: %d failed", sockfd);
		return SOCK_ERROR;
	}

	server = gethostbyname(hostname);
	if (server == NULL) {
		LOG_ERROR(h_errno, "hostname lookup: %s failed", hostname);
		return SOCK_ERROR;
	}

	bzero((char *)&serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
	serv_addr.sin_port = htons(portno);
	if (connect(sockfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
		LOG_ERROR(errno, "connection on socket: %d failed", sockfd);
		return SOCK_ERROR;
	}

	return SUCCESS;
}

int sock_write_phd(const unsigned char msg)
{
	ssize_t n;

	n = write(sockfd, &msg, 1);
	if (n < 0) {
		LOG_ERROR(errno, "writing to socked descriptor: %d failed, n: %zd", sockfd, n);
		return SOCK_ERROR;
	}
	return SUCCESS;
}

int sock_read_phd(unsigned char *result)
{
	ssize_t n;

	bzero(result, 1);
	n = read(sockfd, result, 1);

	if (n < 0) {
		LOG_ERROR(errno, "reading from socket descriptor: %d failed, n: %d", sockfd, n);
		return SOCK_ERROR;
	}

	return SUCCESS;
}

int req_star_distance(float *dist)
{
	int rc;
	rc = sock_write_phd(MSG_REQDIST);
	if (rc != SUCCESS)
		return rc;

	unsigned char result;
	rc = sock_read_phd(&result);
	if (rc != SUCCESS)
		return rc;

	if (result == 255)
		*dist = INFINITY;
	else
		*dist = result / 100.0;
	return SUCCESS;
}

int req_dither_move(const unsigned char move_cmd, unsigned char *phd_result)
{
	int rc = PARM_ERROR;

	if (move_cmd == MSG_MOVE1 || move_cmd == MSG_MOVE2 ||
	    move_cmd == MSG_MOVE3 || move_cmd == MSG_MOVE4 ||
	    move_cmd == MSG_MOVE5) {
		rc = sock_write_phd(move_cmd);
		if (rc != SUCCESS)
			return rc;
		rc = sock_read_phd(phd_result);
	} else
		LOG_WARN("%s", "wrong move command, valid are {1,2,3,4,5}");

	return rc;
}

int wait_for_settle(const float settle_dist, const unsigned int max_wait_sec)
{
	int rc = SUCCESS;
	float dist = 0;
	unsigned int iter_sec = 0;

	do {
		rc = req_star_distance(&dist);
		if (rc != SUCCESS)
			LOG_WARN("%s", "cannot request star distance");
		else
			LOG_TRACE("got star distance: %f px, waiting for settle distance: %f in iteration (%d/%d)",
				  dist, settle_dist, iter_sec, max_wait_sec);

		sleep(1);
		iter_sec++;

		if (iter_sec >= max_wait_sec) {
			LOG_WARN("max number of iteration: %d reached without obtaining desired settle distance %f thus dither failed and terminating",
			       iter_sec, settle_dist);
			return DITHER_FAILED;
		}
	} while (dist > settle_dist && dist != 0);

	return rc;
}

void usage(const char* prgname)
{
	/* Taken from phd2/socket_server.cpp
	   MSG_MOVE1:  // +/- 0.5
	   MSG_MOVE2:  // +/- 1.0
	   MSG_MOVE3:  // +/- 2.0
	   MSG_MOVE4:  // +/- 3.0
	   MSG_MOVE5:  // +/- 5.0
	*/
	printf("usage: %s\n"
	       "\t-m, --movecmd <1,2,3,4,5>, where 1: +/-0.5\n"
	       "\t\t\t\t\t 2: +/-1.0\n"
	       "\t\t\t\t\t 3: +/-2.0\n"
	       "\t\t\t\t\t 4: +/-3.0\n"
	       "\t\t\t\t\t 5: +/-5.0 [default]\n"
	       "\t-s, --settledist <float>, 0.5 pixel [default]\n"
	       "\t-w, --wait <int>, 30 sec [default]\n", prgname);

	exit(SUCCESS);
}

unsigned char get_move_cmd(const int move_cmd, char *move_cmd_human_readable)
{
	switch (move_cmd) {
	case 1: {
		strncpy(move_cmd_human_readable, "+/- 0.5", strlen("+/- 0.5"));
		return(MSG_MOVE1);
		break;
	}
	case 2: {
		strncpy(move_cmd_human_readable, "+/- 1.0", strlen("+/- 1.0"));
		return(MSG_MOVE2);
		break;
	}
	case 3: {
		strncpy(move_cmd_human_readable, "+/- 2.0", strlen("+/- 2.0"));
		return(MSG_MOVE3);
		break;
	}
	case 4: {
		strncpy(move_cmd_human_readable, "+/- 3.0", strlen("+/- 3.0"));
		return(MSG_MOVE4);
		break;
	}
	case 5: {
		strncpy(move_cmd_human_readable, "+/- 5.0", strlen("+/- 5.0"));
		return(MSG_MOVE5);
		break;
	}
	default:
		LOG_WARN("unkown move_cmd: %d", move_cmd);
		return 0;
		break;
	}
}

int main(int argc, char *argv[])
{
	int rc;
	unsigned char result;
	unsigned char move_cmd = 5;
	float settle_dist = 0.5;
	unsigned int max_wait_sec = 30;
	int c;
	char move_cmd_human_readable[8] = {0};

	while (1) {
		static struct option long_options[] = {
			{"movecmd",    required_argument, 0, 'm'},
			{"settledist", required_argument, 0, 's'},
			{"wait",       required_argument, 0, 'w'},
			{"help",       no_argument,       0, 'h'},
			{0, 0, 0, 0}
		};
		/* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long (argc, argv, "m:s:w:",
				 long_options, &option_index);

		/* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c) {
		case 'h':
			usage(argv[0]);
			break;
		case 'm':	/* Move command. */
			move_cmd = get_move_cmd(atoi(optarg), move_cmd_human_readable);
			break;
		case 's':	/* Required settle distance. */
			settle_dist = atof(optarg);
			break;
		case 'w':	/* Wait maximum number of seconds. */
			max_wait_sec = atoi(optarg);
			break;
		default:
			usage(argv[0]);
		}
	}

	LOG_TRACE("%s parameters: move_cmd_human_readable = %s"
		  " settle_dist = %f pixels, max_wait_sec = %d secs",
		  argv[0], move_cmd_human_readable, settle_dist, max_wait_sec);

	rc = connect_socket("localhost", 4300);
	if (rc != SUCCESS)
		goto clean_up;

	/* Wait until mount is settled and gives a pixel distance < settle_dist.
	   Note we could omit this step and jump directly to the dither command, however,
	   it is safe to wait first for a settled mount and then send the dither command. */
	rc = wait_for_settle(settle_dist, max_wait_sec);
	if (rc != SUCCESS)
		goto clean_up;

	/* Request a dither command of +/- pixels and check wether command was successful. */
	rc = req_dither_move(move_cmd, &result);
	LOG_TRACE("send dither move and got dither reply: %d (%s)",
		  result, result == 1 ? "OK" : "FAILED");
	if (rc != SUCCESS || result != 1) {
		rc = DITHER_FAILED;
		goto clean_up;
	}

	rc = wait_for_settle(settle_dist, max_wait_sec);
	if (rc != SUCCESS)
		LOG_WARN("%s", "dithering process failed due to wait for settle failed");
	else
		LOG_TRACE("%s", "dithering process was successful");

	/* At this point in time we are safe to take a new astro image.
	   Dithering command was successful and mount is settled with a pixel distance < settle_dist. */

clean_up:
	close(sockfd);
	return rc;
}
