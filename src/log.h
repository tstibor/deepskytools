/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License version 2 for more details (a copy is included
 * in the LICENSE file that accompanied this code).
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2017, Thomas Stibor <thomas@stibor.net>
 */

#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/types.h>

/* ASCII color codes. */
#define NRM  "\x1B[0m"
#define RED  "\x1B[31m"
#define GRN  "\x1B[32m"
#define YEL  "\x1B[33m"
#define BLU  "\x1B[34m"
#define MAG  "\x1B[35m"
#define CYN  "\x1B[36m"
#define WHT  "\x1B[37m"
#define RESET "\033[0m"

#define MSG_MASK     0x00000007
#define MSG_NO_ERRNO 0x00000010

enum lmsg_level {
	MSG_OFF   = 0,
	MSG_ERROR = 1,
	MSG_WARN  = 2,
	MSG_TRACE = 3,
	MSG_MAX
};

#define LOG_ERROR(_rc, _format, ...)					\
	lmsg(MSG_ERROR, _rc, RED "[ERROR] " RESET			\
	     "%s %s [%s:%s:%d] "_format,				\
	     __TIME__, __DATE__, __FILE__, __func__, __LINE__, ## __VA_ARGS__)

#define LOG_WARN(_format, ...)						\
	lmsg(MSG_WARN | MSG_NO_ERRNO, 0, MAG "[WARN]  " RESET		\
	     "%s %s [%s:%s:%d] "_format,				\
	     __TIME__, __DATE__, __FILE__, __func__, __LINE__, ## __VA_ARGS__)

#define LOG_TRACE(_format, ...)						\
	lmsg(MSG_TRACE | MSG_NO_ERRNO, 0, BLU "[TRACE] " RESET		\
	     "%s %s [%s:%s:%d] "_format,				\
	     __TIME__, __DATE__, __FILE__, __func__, __LINE__, ## __VA_ARGS__)

int lmsg_get_level(void);
void lmsg_set_level(int level);
void lmsg(enum lmsg_level level, int err, const char *fmt, ...);

#endif /* LOG_H */
