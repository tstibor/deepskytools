/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2016, Thomas Stibor <thomas@stibor.net>
 */

#include <iostream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

double calcFwhm(const Mat &mat)
{
    Scalar mean;
    Scalar stddev;
    meanStdDev(mat, mean, stddev);

    // 2 * sqrt(2 * ln) = 2.3548
    // Gaussian fwhm = sigma * 2 * sqrt(2 * ln) and additional scaling.
    return sqrt((sqrt(stddev.val[0]) * 2.3548) / M_PI) * 2;
}

double calcHfd(const Mat &mat)
{
    double mass = 0;
    double flux = 0;
    double pixelVal = 0;

    Scalar _mean = mean(mat);
    Moments m = moments(mat, false);
    Point centroid(m.m10 / m.m00, m.m01 / m.m00);

    for (int y = 0; y < mat.rows; y++) {
	for (int x = 0; x < mat.cols; x++) {
	    pixelVal = (double)mat.at<uchar>(y, x) - _mean[0];
	    pixelVal = pixelVal < 0 ? 0 : pixelVal;

	    // Weight pixelVal with distance from centroid.
	    flux += sqrt( (centroid.x - x) * (centroid.x - x) +
			  (centroid.y - y) * (centroid.y - y)) * pixelVal;
	    mass += pixelVal;
	}
    }
    mass = mass == 0 ? 1 : mass;

    return (flux / mass) * 2;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
	return 1;

    VideoCapture cap(argv[1]);

    if (!cap.isOpened()) {
	cout << "ERROR: Cannot find video file: " << argv[1] << endl;
	return 1;
    }

    Mat frame;
    cap >> frame;
    cout << "frame dimension: " << frame.cols << " x " << frame.rows << endl;

    unsigned long numFrame = 0;
    unsigned int width = 64;
    unsigned int height = 64;
    unsigned int x1 = frame.cols / 2 - width / 2;
    unsigned int y1 = frame.rows / 2 - height / 2;
    unsigned int x2 = frame.cols / 2 + width / 2;
    unsigned int y2 = frame.rows / 2 + height / 2;
    const int thickness = 3;

    char key = 0;
    bool quit = false;
    unsigned int stepSize = 4;
    unsigned int zoom = 2;

    Rect roiRect;
    Rect zoomArea;
    Mat frameRoi;
    Moments mts;

    double *plotVarianceSum = (double *)malloc(sizeof(double) * frame.cols);
    bzero(&plotVarianceSum[0], frame.cols);
    unsigned int xPlot = 0;

    unsigned int ytop = frame.rows - 0.25 * frame.rows;
    unsigned int ybottom = frame.rows;
    unsigned int ycenter = (ybottom - ytop) / 2 + ytop;
    cout << "ytop: " << ytop << endl;
    cout << "ybottom: " << ybottom << endl;
    cout << "ycenter: " << ycenter << endl;

    bool canZoom = false;
    double minFocusVal = DBL_MAX;
    Mat grayMat;
    stringstream focusValStr;

    while (!quit) {
	cap >> frame;
	if (frame.empty())
	    break;

	switch (key = static_cast<char>(waitKey(10))) {
	case '1': {
	    if (zoom > 1)
		zoom--;
	    break;
	}
	case '2': {
	    if (zoom < 10 && canZoom)
		zoom++;
	    break;
	}
	case '+': {
	    x2 += stepSize;
	    y2 += stepSize;
	    x1 -= stepSize;
	    y1 -= stepSize;
	    break;
	}
	case '-': {
	    x2 -= stepSize;
	    y2 -= stepSize;
	    x1 += stepSize;
	    y1 += stepSize;
	    break;
	}
	case 81: { // Left cursor key.
	    x1 -= stepSize;
	    x2 -= stepSize;
	    break;
	}
	case 84: { // Down cursor key.
	    y1 += stepSize;
	    y2 += stepSize;
	    break;
	}
	case 83: { // Right cursor key.
	    x1 += stepSize;
	    x2 += stepSize;
	    break;
	}
	case 82: { // Up cursor key.
	    y1 -= stepSize;
	    y2 -= stepSize;
	    break;
	}
	case 'c': {
	    minFocusVal = DBL_MAX;
	    break;
	}
	case 'q': {
	    quit = true;
	    break;
	}
	case 27: {
	    quit = true;
	    break;
	}
	}

	roiRect = Rect(Point(x1, y1), Point(x2, y2));
	frameRoi = frame(roiRect);

	if ((unsigned int)frameRoi.cols * zoom < (unsigned int)frame.cols &&
	    (unsigned int)frameRoi.rows * zoom < (unsigned int)frame.rows)
	    canZoom = true;
	else
	    canZoom = false;

	cvtColor(frameRoi, grayMat, cv::COLOR_BGR2GRAY);

	resize(frameRoi, frameRoi, Size(frameRoi.cols * zoom, frameRoi.rows * zoom),
	       0, 0, INTER_CUBIC);

	frameRoi.copyTo(frame(cv::Rect(0, 0, frameRoi.cols, frameRoi.rows)));
	rectangle(frame, Point(0, 0), Point(frameRoi.cols, frameRoi.rows),
		  CV_RGB(0, 255, 0), 1, 8, 0);

	putText(frame, "zoom: " + to_string(zoom) + "x",
		Point(frameRoi.cols - 85, frameRoi.rows - 2), FONT_HERSHEY_COMPLEX, 0.5,
		Scalar(0, 255, 0), 1);

	// draw ROI rectangle.
	rectangle(frame, Point(x1, y1), Point(x2, y2),
		  CV_RGB(255, 0, 0), thickness, 8, 0);

	// Draw fwhm/hfd string value below ROI rectangle.
	double focusVal = calcHfd(grayMat);
	focusValStr << setprecision (2) << fixed << focusVal;
	putText(frame, "hfd: " + focusValStr.str(), Point(x2 - (x2 - x1), y2 + 40),
		FONT_HERSHEY_COMPLEX, 1, Scalar(255, 255, 255), 2);
	focusValStr.str("");

	if (focusVal < minFocusVal)
	    minFocusVal = focusVal;

	focusValStr << setprecision (2) << fixed << minFocusVal;
	putText(frame, "min hfd: " + focusValStr.str(), Point(0, ytop - 5),
		FONT_HERSHEY_COMPLEX, 1, Scalar(255, 255, 255), 2);
	focusValStr.str("");

	if (xPlot >= (unsigned int)frame.cols)
	    xPlot = 0;

	unsigned int scaleOffset = 0;
	plotVarianceSum[xPlot++] = scaleOffset + focusVal;
	if (numFrame > 2 && xPlot > 1) {
	    for (unsigned int p = 0; p < xPlot - 1; p++)
		line(frame, Point(p, ybottom + scaleOffset - plotVarianceSum[p]),
		     Point(p + 1, ybottom + scaleOffset - plotVarianceSum[p + 1]), Scalar(255, 255, 255), 2);

	}
	// Plot grid.
	for (unsigned int ypos = ytop; ypos < ybottom; ypos += 35)
	    line(frame, Point(0, ypos), Point(frame.cols, ypos), CV_RGB(255, 0, 0), 1);
	for (unsigned int xpos = 0; xpos < (unsigned int)frame.cols; xpos += 35)
	    line(frame, Point(xpos, ytop), Point(xpos, ybottom), CV_RGB(255, 0, 0), 1);

	imshow("My", frame);
	numFrame++;
    }

    cap.release();
    free(plotVarianceSum);

    return 0;
}
