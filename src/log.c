/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2017, Thomas Stibor <thomas@stibor.net>
 */

#include "log.h"

static unsigned int lmsg_level = MSG_MAX;

static void log_callback(enum lmsg_level level, int err,
			 const char *fmt, va_list ap)
{
	vfprintf(stderr, fmt, ap);
	if (level & MSG_NO_ERRNO || !err)
		fprintf(stderr, "\n");
	else
		fprintf(stderr, ": %s (%d)\n", strerror(err), err);
}

void lmsg(enum lmsg_level level, int err, const char *fmt, ...)
{
	va_list args;
	int tmp_errno = errno;

	if ((level & MSG_MASK) > lmsg_level)
		return;

	va_start(args, fmt);
	log_callback(level, abs(err), fmt, args);
	va_end(args);
	errno = tmp_errno;

}

int lmsg_get_level(void)
{
	return lmsg_level;
}

void log_msg_set_level(int level)
{
	/* Ensure level is in the good range. */
	if (level < MSG_OFF)
		lmsg_level = MSG_OFF;
	else if (level > MSG_MAX)
		lmsg_level = MSG_MAX;
	else
		lmsg_level = level;
}
