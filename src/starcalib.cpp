/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Copyright (c) 2016, Thomas Stibor <thomas@stibor.net>
 */

#include <iostream>
#include <string>
#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

static const int fontScale = 1;
static const int thickness = 2;
static const unsigned int stepSize = 4;
static unsigned int xPlot = 0;
static unsigned long frameCounter = 0;
static double *plotVarianceSum = NULL;
static unsigned int roi_x1;
static unsigned int roi_y1;
static unsigned int roi_x2;
static unsigned int roi_y2;

vector<string> extractRadioSettings(const char *list, const string &findStr)
{
    vector<string> settingsVec;
    const string listStr(list);
    size_t found;

    found = listStr.find(findStr);
    if (found == string::npos)
	return settingsVec;

    size_t posOpenBracket = listStr.find('(', found);
    if (posOpenBracket == string::npos)
    	return settingsVec;

    size_t posClosedBracket = listStr.find(')', posOpenBracket);
    if (posClosedBracket == string::npos)
	return settingsVec;

    posOpenBracket++;
    size_t posColon = posOpenBracket + 1;
    size_t posSemiColon;

    while (posColon < posClosedBracket) {
	while (listStr[posColon++] != ':');
	posSemiColon = posColon + 1;
	while (listStr[posSemiColon++] != ';' && listStr[posSemiColon - 1] != ')');
	string val = listStr.substr(posColon, posSemiColon - (posColon + 1));
	settingsVec.push_back(val);
	posColon = posSemiColon + 1;
    }
    posSemiColon = posColon;
    while (listStr[posSemiColon++] != ',');

    // Current value is last element in vector.
    string curValStr = listStr.substr(posColon, posSemiColon - (posColon + 1 ));
    settingsVec.push_back(curValStr);

    return settingsVec;
}

double calcFwhm(const Mat &mat)
{
    Scalar mean;
    Scalar stddev;
    meanStdDev(mat, mean, stddev);

    // 2 * sqrt(2 * ln) = 2.3548
    // Gaussian fwhm = sigma * 2 * sqrt(2 * ln) and additional scaling.
    return sqrt((sqrt(stddev.val[0]) * 2.3548) / M_PI) * 2;
}

void overlayFwhm(Mat &frame)
{
    const int thickness = 3;
    const unsigned int ytop = frame.rows - 0.25 * frame.rows;
    const unsigned int ybottom = frame.rows;

    Rect roiRect = Rect(Point(roi_x1, roi_y1), Point(roi_x2, roi_y2));
    Mat frameRoi = frame(roiRect);

    Mat grayMat;
    cvtColor(frameRoi, grayMat, cv::COLOR_BGR2GRAY);

    // draw ROI rectangle.
    rectangle(frame, Point(roi_x1, roi_y1), Point(roi_x2, roi_y2), CV_RGB(255, 0, 0), thickness, 8, 0);

    // draw fwhm value below ROI rectangle.
    std::stringstream fwhmStr;
    double fwhmVal = calcFwhm(grayMat);
    fwhmStr << setprecision (2) << fixed << fwhmVal;
    putText(frame, "fwhm: " + fwhmStr.str(), Point(roi_x2 - (roi_x2 - roi_x1), roi_y2 + 40),
	    FONT_HERSHEY_COMPLEX, 1, Scalar(255, 255, 255), 2);


    if (xPlot >= (unsigned int)frame.cols)
	xPlot = 0;

    unsigned int scaleOffset = 35;
    plotVarianceSum[xPlot++] = scaleOffset * fwhmVal;
    if (frameCounter > 2 && xPlot > 1) {
	for (unsigned int p = 0; p < xPlot - 1; p++)
	    line(frame, Point(p, ybottom + scaleOffset - plotVarianceSum[p]),
		 Point(p + 1, ybottom + scaleOffset - plotVarianceSum[p + 1]), Scalar(255, 255, 255), 2);

    }
    // Plot grid.
    for (unsigned int ypos = ytop; ypos < ybottom; ypos += 35)
	line(frame, Point(0, ypos), Point(frame.cols, ypos), CV_RGB(255, 0, 0), 1);
    for (unsigned int xpos = 0; xpos < (unsigned int)frame.cols; xpos += 35)
	line(frame, Point(xpos, ytop), Point(xpos, ybottom), CV_RGB(255, 0, 0), 1);

}

void displayHelp(Mat &frame)
{
    const string keys[] = {"h toggle help screen",
			   "1 decrease ISO",
			   "2 increase ISO",
			   "3 decrease shutterspeed",
			   "4 increase shutterspeed",
			   "v toggle value screen ISO and shutterspeed",
			   "+ increase FWHM square",
			   "- decrease FWHM square",
			   "→ move FWHM square right",
			   "← move FWHM square left",
			   "↑ move FWHM square up",
			   "↓ move FWHM square down"};
    const int N = sizeof(keys)/sizeof(keys[0]);

    for (int i = 0; i < N; i++)
	putText(frame, keys[i], Point(10, 30 + 30 * i),
		FONT_HERSHEY_COMPLEX, fontScale, Scalar(255, 255, 255), thickness);

}

int main(int argc, char *argv[])
{
    const string windowName = "StarCalib";
    string deviceName = "Canon";
    if (argc == 2)
	deviceName = argv[1];

    VideoCapture cap(deviceName);

    Mat frame;

    bool showIsoShutterspeed = true;
    bool showHelp = false;

    int isoIndex = 0;
    int shutterSpeedIndex = 0;

    if (!cap.isOpened()) {
	cout << "ERROR: Cannot find device: " << deviceName << endl;
	return 1;
    }
    if ((cap.get(CAP_PROP_GPHOTO2_WIDGET_ENUMERATE) == 0)
	|| (cap.get(CAP_PROP_GPHOTO2_WIDGET_ENUMERATE) == -1)) {
	// Some VideoCapture implementations can return -1, 0.
	cout << "ERROR: This is not a GPHOTO2 device" << endl;
	return 2;
    }
    const char* gphoto2ConfigList = (const char *)(intptr_t)cap.get(CAP_PROP_GPHOTO2_WIDGET_ENUMERATE);
#ifdef DEBUG
    cout << "List of camera settings: " << endl
	 << gphoto2ConfigList << endl;
#endif
    cap.set(CAP_PROP_GPHOTO2_COLLECT_MSGS, true);
    cap.set(CAP_PROP_GPHOTO2_PREVIEW, true);

    vector<string> isoVec = extractRadioSettings(gphoto2ConfigList, "ISO Speed");
    string isoCur = isoVec.back();
    isoVec.pop_back();
    for (auto& element : isoVec) {
	if (isoCur == element) {
	    break;
	}
	isoIndex++;
    }

    vector<string> sspeedVec = extractRadioSettings(gphoto2ConfigList, "Shutter Speed");
    string sspeedCur = sspeedVec.back();
    sspeedVec.pop_back();
    for (auto& element : sspeedVec) {
	if (sspeedCur == element) {
	    break;
	}
	shutterSpeedIndex++;
    }

    namedWindow(windowName, CV_WINDOW_NORMAL);
    setWindowProperty(windowName, CV_WND_PROP_FULLSCREEN, CV_WINDOW_FULLSCREEN);

    cap >> frame;
    plotVarianceSum = (double *)malloc(sizeof(double) * frame.cols);
    bzero(&plotVarianceSum[0], frame.cols);
    unsigned int width = 64;
    unsigned int height = 64;
    roi_x1 = frame.cols / 2 - width / 2;
    roi_y1 = frame.rows / 2 - height / 2;
    roi_x2 = frame.cols / 2 + width / 2;
    roi_y2 = frame.rows / 2 + height / 2;

    char key = 0;
    while (key != 'q' && key != 27 /*ESC*/) {

	cap >> frame;
        if (frame.empty())
            break;

	switch (key = static_cast<char>(waitKey(10)))
	{
	case 'h':
	    showHelp = !showHelp;
	    break;
	case '1': // decrease ISO value
	    if (isoIndex > 0) {
		cap.set(CAP_PROP_ISO_SPEED, --isoIndex);
	    }
	    break;
	case '2': // increase ISO value
	    if (isoIndex < (int)isoVec.size() - 1) {
		cap.set(CAP_PROP_ISO_SPEED, ++isoIndex);
	    }
	    break;
	case '3': // decrease shutterspeed value
	    if (shutterSpeedIndex > 0) {
		cap.set(CAP_PROP_SPEED, --shutterSpeedIndex);
	    }
	    break;
	case '4': // increase shutterspeed value
	    if (shutterSpeedIndex < (int)sspeedVec.size() - 1) {
		cap.set(CAP_PROP_SPEED, ++shutterSpeedIndex);
	    }
	    break;
	case 'v': // toggle show ISO and shutterspeed values
	    showIsoShutterspeed = !showIsoShutterspeed;
	    break;
	case '+': {
	    roi_x2 += stepSize;
	    roi_y2 += stepSize;
	    roi_x1 -= stepSize;
	    roi_y1 -= stepSize;
	    break;
	}
	case '-': {
	    roi_x2 -= stepSize;
	    roi_y2 -= stepSize;
	    roi_x1 += stepSize;
	    roi_y1 += stepSize;
	    break;
	}
	case 81: { // Left cursor key.
	    roi_x1 -= stepSize;
	    roi_x2 -= stepSize;
	    break;
	}
	case 84: { // Down cursor key.
	    roi_y1 += stepSize;
	    roi_y2 += stepSize;
	    break;
	}
	case 83: { // Right cursor key.
	    roi_x1 += stepSize;
	    roi_x2 += stepSize;
	    break;
	}
	case 82: { // Up cursor key.
	    roi_y1 -= stepSize;
	    roi_y2 -= stepSize;
	    break;
	}
	}

	if (showHelp)
	    displayHelp(frame);
	else {
	    if (showIsoShutterspeed) {
		const int xBackSize = 260;
		putText(frame, "ISO: " + isoVec.at(isoIndex), Point(frame.size().width - xBackSize, 30),
			FONT_HERSHEY_COMPLEX, fontScale, Scalar(255, 255, 255), thickness);
		putText(frame, "Speed: " + sspeedVec.at(shutterSpeedIndex), Point(frame.size().width - xBackSize, 60),
			FONT_HERSHEY_COMPLEX, fontScale, Scalar(255, 255, 255), thickness);
	    }
	}

	overlayFwhm(frame);

	imshow(windowName, frame);
	frameCounter++;
    } // End while.

    cout << "Captured " << frameCounter << " frames"
	 << " in " << (int)(cap.get(CAP_PROP_POS_MSEC) / 1e2)
	 << " seconds," << endl << "at avg speed "
	 << (cap.get(CAP_PROP_FPS)) << " fps" << endl
	 << "Size " << frame.size().width << " x " << frame.size().height << endl
	 << "ISO " << isoVec.at(isoIndex) << endl
	 << "Shutterspeed " << sspeedVec.at(shutterSpeedIndex) << endl;

    cap.release();
    if (plotVarianceSum)
	free(plotVarianceSum);

    return 0;
}
