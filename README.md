# DeepSkyTools #

## Introduction
DeepSkyTools is a collection of Bash scripts and C/C++ programs for:

* Taking long exposure astronomy images with [GPhoto2](http://gphoto.sourceforge.net/).
* Taking exposure preview astronomy images with [GPhoto2](http://gphoto.sourceforge.net/) and [FEH](https://feh.finalrewind.org/).
* Platesolving with [Astrometry.net](http://astrometry.net/) the taken images.
* Sending the extracted RA,DEC coordinates to Celestron mounts via library [libnexstar](https://sourceforge.net/projects/libnexstar/) for astrometric mount calibration.
* Polar alignment based on three taken and solved images (see [http://lists.astrometry.net/pipermail/astrometry/2010-June/001811.html](http://lists.astrometry.net/pipermail/astrometry/2010-June/001811.html))
* Liveview and arbitrary zoom level via [OpenCV](http://opencv.org) connected to Canon DSLR camera supported by [GPhoto2](http://gphoto.sourceforge.net/).
* Simple program based on [xcalib](http://xcalib.sourceforge.net/) for switching in circular direction between low screen brightness -> red screen -> original screen brightness.
* Simple GUI realized with [yad](https://sourceforge.net/projects/yad-dialog).

## Taking long exposures with DSLR and mirror lockup
Long exposure images are taken with bash function which employs _yad_ for creating the graphical dialog and _gphoto2_ for taking the long exposure image:

```
#!bash
__gphoto_bulb_mlu() { # Strange hex code to enable mirror lockup.
    ${GPHOTO2_BIN} --set-config customfuncex=20,1,3,14,1,60f,1,1 \
		   --set-config capturetarget=0 \
		   --set-config eosremoterelease=2 \
		   --set-config eosremoterelease=4 \
		   --wait-event=${3}s \
		   --set-config eosremoterelease=2 \
		   --wait-event=${1}s \
		   --set-config eosremoterelease=4 \
		   --set-config eosremoterelease=0 \
		   --capture-tethered=2s \
		   --wait-event-and-download=2s \
		   --force-overwrite \
		   --filename ${2}
    [[ ! $? -eq 0 ]] && { ERRMSG="gphoto2 taking mlu image failed"; \
			  yad --center \
			      --gtkrc="./skytools_gtk.rc" \
			      --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			      --text="${ERRMSG}"; exit 1; }
}
```
Note there is no F-stop option since this is developed for astroimaging.

![takeimage.png](https://bitbucket.org/repo/Lgg4ea/images/938351306-takeimage.png)
 
## Platesolving astronomy image and sending RA,DEC coordinates to Celestron mount
One of the core functionality of deepskytools is the ability to platesolve images and sending the extracted
RA, DEC coordinates to a Celestron mount, thus the mount knows precisely where the telescope is pointed. This
feature saves a lot of time calibrating the mount and is far more precise than the classical 2 or 3 star calibration.
For platesolving an image it uses the great and amazing [astrometry.net](http://nova.astrometry.net) software engine.

 
## Liveview and arbitrary zoom level with OpenCV and Canon DSLR
An additional program in the deepskytools set is the OpenCV program called _starcalib_. For compiling _starcalib.cpp_
one requires the latest GPhoto2 library and OpenCV >= 3.0 which is compiled with GPhoto2 support.
Starting the program shows the liveview screen of a DSLR (supported by GPhoto2 and with liveview capability).
For demonstrating the features of starcalib click on the following link [starcalib.mp4](http://www.stibor.net/screencasts/starcalib.mp4) to see the video. In the video I am filming the "Himmelskalender 2016" to highlight the features. Note, at time 1:23 I put the lens cap on and increase the ISO value to the maximum. One can nicely observe the noise and the Bayer matrix.
Here is also a screenshot of the setup and the liveview screen:![Untitled1.png](https://bitbucket.org/repo/Lgg4ea/images/123869682-Untitled1.png)