#!/bin/bash
# Title       : platesolve.sync.mount.sh
# Date        : Sun 20 Mar 2016 11:09:22 AM CET
# Version     : 0.0.3
# Author      : "Thomas Stibor" <thomas@stibor.net>
# Description : 1.) Take image with gphoto2
#               2.) Platesolve it with astrometry.net
#               3.) Send RA,DEC coordinates with libnexstar to mount

source libgphoto.sh
source libsettings.sh
source libastrometry.sh

__focalmm2arcsecpp() {
    echo "scale=4;4.3/${1} * 206.3" | bc
}

__mount_ctrl_sync_radec() {
    ${MOUNTCTRL_BIN} "/dev/ttyUSB0" "syncradec" ${1} ${2}
}

# main
GPHOTO2_BIN=`which gphoto2`
__check_bin ${GPHOTO2_BIN} "gphoto2"

AN_BIN=`which solve-field`
__check_bin ${AN_BIN} "solve-field"

MOUNTCTRL_BIN="bin/mountctrl"
__check_bin ${MOUNTCTRL_BIN} "bin/mountctrl"

EXP_VALS=$(__read_settings "exposure")
ISO_VALS=$(__read_settings "iso")
ALO_VALS=$(__read_settings "applow")
AHI_VALS=$(__read_settings "apphigh")
DWS_VALS=$(__read_settings "downsample")
CPU_VALS=$(__read_settings "cpulimit")

result=$(yad --undecorated --title="Solve and Sync" --form --item-separator="," \
	     --center \
	     --gtkrc="./skytools_gtk.rc" \
	     --field="Exposure (sec)":CB  ${EXP_VALS} \
	     --field="ISO":CB ${ISO_VALS} \
	     --field="Arcsec/pix low":CB ${ALO_VALS} \
	     --field="Arcsec/pix high":CB ${AHI_VALS} \
	     --field="Downsample":CB ${DWS_VALS} \
	     --field="Cpulimit (sec)":CB ${CPU_VALS} \
	     --button="Solve and Sync:2" \
	     --button="gtk-cancel:1")

ret=$?
[[ $ret -eq 1 ]] && { exit 1; }

array=(${result//|/ })

#### DEBUG START ####
# echo "DEBUG: ${result}"
# for i in "${!array[@]}"
# do
#     echo "$i=>${array[i]}"
# done
#### DEBUG END ####

IMG_FILENAME="/tmp/tosolve.jpg"
AN_PARAM_STD="--no-plots --no-fits2fits --no-verify --overwrite --scale-units arcsecperpix"

GPHOTO2_EXP_SEC=${array[0]}
__update_selected "exposure" ${GPHOTO2_EXP_SEC}

GPHOTO2_ISO=${array[1]}
__update_selected "iso" ${GPHOTO2_ISO}

AN_APP_L=" --scale-low ${array[2]}"
__update_selected "applow" ${array[2]}

AN_APP_H=" --scale-high ${array[3]}"
__update_selected "apphigh" ${array[3]}

AN_DOWNSAMPLE=" --downsample ${array[4]}"
__update_selected "downsample" ${array[4]}

AN_CPULIMIT=" --cpulimit ${array[5]}"
__update_selected "cpulimit" ${array[5]}

######## Set camera settings and take image ########
__gphoto_set_config "imageformat=6"
__gphoto_set_config "shutterspeed=bulb"
__gphoto_set_config "iso=${GPHOTO2_ISO}"
__gphoto_bulb "${GPHOTO2_EXP_SEC}" "${IMG_FILENAME}" \
    | yad --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 \
	  --title="Taking image for being plate solved..." \
	  --center \
	  --gtkrc="./skytools_gtk.rc" \
	  --text="ISO: ${GPHOTO2_ISO}\nEXP: ${GPHOTO2_EXP_SEC} sec"

echo "${AN_BIN}" "${AN_PARAM_STD}" "${AN_APP_L}" "${AN_APP_H}" "${AN_DOWNSAMPLE}" "${AN_CPULIMIT}" "${IMG_FILENAME}"

######## Solve image with astrometry.net ########
RA_DEC=$(__solve_img "${AN_BIN}" "${AN_PARAM_STD}" "${AN_APP_L}" "${AN_APP_H}" "${AN_DOWNSAMPLE}" "${AN_CPULIMIT}" "${IMG_FILENAME}")
[[ -z ${RA_DEC} ]] && { killall -9 astrometry-engine 2> /dev/null; exit 1; }
# echo "RA,DEC: ${RA_DEC}"
RA_DEC=(${RA_DEC//' '/ })

######## Send RA,DEC coordinates to mount ########
ERRMSG=$(__mount_ctrl_sync_radec "${RA_DEC[0]}" "${RA_DEC[1]}")
ret=$?
[[ $ret -eq 1 ]] && { yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
                          --gtkrc="./skytools_gtk.rc" \
			  --text="${ERRMSG}"; exit 1; }
{ yad --button="gtk-ok:0" --image=info \
      --center \
      --gtkrc="./skytools_gtk.rc" \
      --text="Successfully send (RA,DEC):(${RA_DEC[0]},${RA_DEC[1]}) coordinates to mount"; exit 0; }
