#!/bin/bash

SETTINGS_FILE="settings.txt"

__check_bin() {
    [[ ! -f ${1} ]] && { ERRMSG="Cannot find ${1} binary"; \
			 yad --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			     --center --gtkrc="./skytools_gtk.rc" \
			     --text="${ERRMSG}"; exit 1; }
}

__read_settings() {
    echo `cat ${SETTINGS_FILE} | grep -oP "(?<=^${1}:).*$"`
}

__update_selected() {
    # Replace "^" by ""
    sed -i "/${1}/s/\^//g" ${SETTINGS_FILE}

    # Replace ":<VAL>," by ":^<VAL>,"
    sed -i '/'"${1}"'/s/':"${2}",'/':"^${2}",'/g' ${SETTINGS_FILE}

    # Replace ",<VAL>," by ",^<VAL>,"
    sed -i '/'"${1}"'/s/',"${2}",'/',"^${2}",'/g' ${SETTINGS_FILE}

    # Replace ",<VAL>$" by ",<VAL>$"
    sed -i '/'"${1}"'/s/',"${2}"\$'/',"^${2}"'/g' ${SETTINGS_FILE}
}
