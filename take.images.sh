#!/bin/bash

# Title	      : take.images.sh
# Date	      : Sun 15 Jan 2017 09:25:47 AM CET
# Version     : 0.0.5
# Author      : "Thomas Stibor" <thomas@stibor.net>
# Description : 1.) Take N long exposure images with gphoto2 and mirror lookup.
#             : 2.) Take bias and dark frames (flat is work in progress).
#		3.) Send dither command via phddither to PHD2 guiding.
# Note	      : Install feh image viewer.

source libgphoto.sh
source libsettings.sh

# main
GPHOTO2_BIN=`which gphoto2`
__check_bin ${GPHOTO2_BIN} "gphoto2"

PHDDITHER_BIN="bin/phddither2"
__check_bin ${PHDDITHER_BIN} "bin/phddither2"

LONGEXP_VALS=$(__read_settings "longexposure")
ISO_VALS=$(__read_settings "iso")
MLU_VALS=$(__read_settings "mlu")
NUM_IMAGES_VALS=$(__read_settings "numimages")
DITHER_VALS=$(__read_settings "dither")
DITHERPIXEL_VALS=$(__read_settings "ditherpixel")
SETTLEDIST_VALS=$(__read_settings "settledist")
MINTIMESTABLE_VALS=$(__read_settings "mintimestable")
TIMEOUT_VALS=$(__read_settings "timeout")

result=$(yad --undecorated --title="Capture Images" --form --item-separator="," \
	     --center \
	     --gtkrc="./skytools_gtk.rc" \
	     --field="Exposure (sec)":CB ${LONGEXP_VALS} \
	     --field="ISO":CB ${ISO_VALS} \
	     --field="MLU (sec)":CB ${MLU_VALS} \
	     --field="Number of images":CB ${NUM_IMAGES_VALS} \
	     --field="Dither":CB ${DITHER_VALS} \
	     --field="Dither pixel":CB ${DITHERPIXEL_VALS} \
	     --field="Settle distance (px)":CB ${SETTLEDIST_VALS} \
	     --field="Mininum time stable (sec)":CB ${MINTIMESTABLE_VALS} \
	     --field="Timeout (sec)":CB ${TIMEOUT_VALS} \
	     --button="Preview:100" \
	     --button="Science:110" \
	     --button="Dark:120" \
	     --button="Flat:130" \
	     --button="Bias:140" \
	     --button="gtk-cancel:1")

ret=$?
[ $ret -eq 1 ] && exit ${ret}

array=(${result//|/ })

GPHOTO2_LONGEXP_SEC=${array[0]}
__update_selected "longexposure" ${GPHOTO2_LONGEXP_SEC}

GPHOTO2_ISO=${array[1]}
__update_selected "iso" ${GPHOTO2_ISO}

GPHOTO2_MLU=${array[2]}
__update_selected "mlu" ${GPHOTO2_MLU}

NUM_IMAGES=${array[3]}
__update_selected "numimages" ${NUM_IMAGES}

BOOL_DITHER=${array[4]}
__update_selected "dither" ${BOOL_DITHER}

DITHERPIXEL=${array[5]}
__update_selected "ditherpixel" ${DITHERPIXEL}

SETTLEDIST=${array[6]}
__update_selected "settledist" ${SETTLEDIST}

MINTIMESTABLE=${array[7]}
__update_selected "mintimestable" ${MINTIMESTABLE}

TIMEOUT=${array[8]}
__update_selected "timeout" ${TIMEOUT}

FRAME_TYPE=""
FILENAME_PART=""

case "${ret}" in
    100) FRAME_TYPE="preview"
	 ;;
    110) FRAME_TYPE="science"
	 ;;
    120) FRAME_TYPE="dark"
	 ;;
    130) FRAME_TYPE="flat"
	 # Note, make sure exposure is set to any value except bulb
	 GPHOTO2_LONGEXP_SEC=`__gphoto_get_config "shutterspeed" | tr "/" "_"`
	 ;;
    140) FRAME_TYPE="bias"
	 __gphoto_set_config "shutterspeed=1/4000"
	 GPHOTO2_LONGEXP_SEC="1_4000" # Manually modify to fastest possible shutter speed string.
	 ;;
esac

######## Set camera settings and take image ########
__gphoto_set_config "imageformat=9"
__gphoto_set_config "iso=${GPHOTO2_ISO}"
[ ${ret} -ne 130 ] && { __gphoto_set_config "shutterspeed=bulb"; }

FILENAME_PART="${FRAME_TYPE}_iso_${GPHOTO2_ISO}_exp_${GPHOTO2_LONGEXP_SEC}.CR2"

# If Preview is chosen, take one image only.
if [ $ret -eq 100 ]
then
    NUM_IMAGES=1
fi

for (( n=1; n<=${NUM_IMAGES}; n++ ))
do
    FILENAME="`date +"%Y.%m.%d-%H:%M:%S"`_${FILENAME_PART}"
    if [ ${ret} -eq 110 ] # Science image with mirror lockup.
    then
	__gphoto_bulb_mlu "${GPHOTO2_LONGEXP_SEC}" ${FILENAME} "${GPHOTO2_MLU}" \
	    | yad --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 \
		  --title="Taking ${FRAME_TYPE} image with mirror lockup..." \
		  --center \
		  --gtkrc="./skytools_gtk.rc" \
		  --text="Taking image ${n} / ${NUM_IMAGES}\nISO: ${GPHOTO2_ISO}\nEXP: ${GPHOTO2_LONGEXP_SEC} sec\nMLU: ${GPHOTO2_MLU} sec"
	if [ "${BOOL_DITHER}" == "TRUE" ]
	then
	    ${PHDDITHER_BIN} -d ${DITHERPIXEL} -s ${SETTLEDIST} -m ${MINTIMESTABLE} -t ${TIMEOUT};
	    rc_phddither=$?;
	    case "${rc_phddither}" in
		1) PHD_MSG="dither failed"
		   ;;
		2) PHD_MSG="wrong parameters"
		   ;;
		3) PHD_MSG="socket error"
		   ;;
	    esac
	    if [ ! ${rc_phddither} -eq 0 ]
	    then
		yad --center \
		    --gtkrc="./skytools_gtk.rc" \
		    --image=error --width=300 --title 'Error' --button="gtk-close:1" \
		    --text="${PHD_MSG}"
	    fi
	fi
    elif [ ${ret} -eq 130 ] || [ ${ret} -eq 140 ]; # Flat or bias image.
    # Note make sure, the exposure time is properly set for flat, e.g. by inspecting the histogram.
    then
	__gphoto_exp ${FILENAME} \
	    | yad --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 \
		  --title="Taking ${FRAME_TYPE} image..." \
		  --center \
		  --gtkrc="./skytools_gtk.rc" \
		  --text="Taking image ${n} / ${NUM_IMAGES}\nISO: ${GPHOTO2_ISO}\nEXP: ${GPHOTO2_LONGEXP_SEC} sec"
    else
	__gphoto_bulb "${GPHOTO2_LONGEXP_SEC}" ${FILENAME} \
	    | yad --progress --pulsate --auto-close --auto-kill --button gtk-cancel:1 \
		  --title="Taking ${FRAME_TYPE} image..." \
		  --center \
		  --gtkrc="./skytools_gtk.rc" \
		  --text="Taking image ${n} / ${NUM_IMAGES}\nISO: ${GPHOTO2_ISO}\nEXP: ${GPHOTO2_LONGEXP_SEC} sec"
    fi
    if [ $ret -eq 100 ] # Preview image with feh image viewer.
    then
	feh -Z -F ${FILENAME}
    fi
done
