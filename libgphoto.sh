#!/bin/bash

__gphoto_set_config() {
    ${GPHOTO2_BIN} --set-config $1
    [[ ! $? -eq 0 ]] && { ERRMSG="gphoto2 set-config $1 failed"; \
			  yad --center \
			      --gtkrc="./skytools_gtk.rc" \
			      --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			      --text="${ERRMSG}"; exit 1; }
}

__gphoto_get_config() {
    local RESULT=`${GPHOTO2_BIN} --get-config $1 | awk '/Current:/{print $2}'`
    [[ ! $? -eq 0 ]] && { ERRMSG="gphoto2 get-config $1 failed"; \
			  yad --center \
			      --gtkrc="./skytools_gtk.rc" \
			      --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			      --text="${ERRMSG}"; exit 1; }
    echo ${RESULT}
}

# Take image with disabled mirror lockup.
# Parameters: ${1} filename of taken image.
__gphoto_exp() { # Strange hex code to disable mirror lockup.
    ${GPHOTO2_BIN} --set-config customfuncex=20,1,3,14,1,60f,1,0 \
		   --capture-image-and-download \
		   --force-overwrite \
		   --filename ${1}
    [[ ! $? -eq 0 ]] && { ERRMSG="gphoto2 take image failed"; \
			  yad --center \
			      --gtkrc="./skytools_gtk.rc" \
			      --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			      --text="${ERRMSG}"; exit 1; }
}

# Take image with disabled mirror lockup.
# Parameters: ${1} exposure time in seconds,
#             ${2} filename of taken image.
__gphoto_bulb() { # Strange hex code to disable mirror lockup.
    ${GPHOTO2_BIN} --set-config customfuncex=20,1,3,14,1,60f,1,0 \
		   --set-config eosremoterelease=5 \
		   --wait-event=${1}s \
		   --set-config eosremoterelease=11 \
		   --wait-event-and-download=4s \
		   --force-overwrite \
		   --filename ${2}
    [[ ! $? -eq 0 ]] && { ERRMSG="gphoto2 take image failed"; \
			  yad --center \
			      --gtkrc="./skytools_gtk.rc" \
			      --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			      --text="${ERRMSG}"; exit 1; }
}

# Take image with enabled mirror lockup.
# Parameters: ${1} exposure time in seconds,
#             ${2} filename of taken image.
#             ${3} mirror lockup time in seconds (recommended 10 seconds),
__gphoto_bulb_mlu() { # Strange hex code to enable mirror lockup.
    ${GPHOTO2_BIN} --set-config customfuncex=20,1,3,14,1,60f,1,1 \
		   --set-config capturetarget=0 \
		   --set-config eosremoterelease=2 \
		   --set-config eosremoterelease=4 \
		   --wait-event=${3}s \
		   --set-config eosremoterelease=2 \
		   --wait-event=${1}s \
		   --set-config eosremoterelease=4 \
		   --set-config eosremoterelease=0 \
		   --capture-tethered=2s \
		   --wait-event-and-download=2s \
		   --force-overwrite \
		   --filename ${2}
    [[ ! $? -eq 0 ]] && { ERRMSG="gphoto2 taking mlu image failed"; \
			  yad --center \
			      --gtkrc="./skytools_gtk.rc" \
			      --image=error --width=300 --title 'Error' --button="gtk-close:1" \
			      --text="${ERRMSG}"; exit 1; }
}
